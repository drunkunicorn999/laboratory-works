from functions_of_labs import g_func, f_func, y_func


while True:

    x_list = [[], [], []]
    y_list = [[], [], []]

    x = float(input('Введите значение переменной x для функции: '))
    a = float(input('Введите значение переменной a для функции: '))
    x_max = float(input('Введите максимальное значение x: '))
    x_step = float(input('Введите количество шагов: '))

    while x < x_max:
        try:
            g = g_func(x, a)
            x_list[0].append(x), y_list[0].append(g)
            x += x_step
        except ZeroDivisionError:
            x_list[0].append(x), y_list[0].append(None)
        try:
            f = f_func(x, a)
            x_list[1].append(x), y_list[1].append(f)
            x += x_step
        except OverflowError or ValueError:
            x_list[1].append(x), y_list[1].append(None)
        try:
            y = y_func(x, a)
            x_list[2].append(x), y_list[2].append(y)
            x += x_step
        except ValueError:
            x_list[2].append(x), y_list[2].append(None)

    print(f'x_list: {x_list[0]}\ny_list: {y_list[0]}\n')
    print(f'x_list: {x_list[1]}\ny_list: {y_list[1]}\n')
    print(f'x_list: {x_list[2]}\ny_list: {y_list[2]}\n')

    re = input('Если вы хотите выйти, нажмите yes, в ином случае наберите любое другое значение\n')
    if re == 'yes':
        break
    else:
        print('Программа повторяется заново...')
        continue
